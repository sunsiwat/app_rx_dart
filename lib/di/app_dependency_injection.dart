import 'package:app_rx_dart/controller/home_controller.dart';
import 'package:get_it/get_it.dart';

class AppDependencyInjection{
  /// private constructor
  AppDependencyInjection._internal();

  /// provides d global point of access to class xxx
  static final AppDependencyInjection instance = AppDependencyInjection._internal();

  factory AppDependencyInjection(){
    return instance;
  }

  GetIt get getItInstance => GetIt.instance;

  void init(){
    _registerControllerDependency();
  }

  void _registerControllerDependency(){
    getItInstance.registerFactory<HomeController>(() => HomeController());
    // getItInstance.registerSingleton<HomeController>(HomeController());
  }
}