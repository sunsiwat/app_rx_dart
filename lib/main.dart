import 'dart:developer';

import 'package:app_rx_dart/controller/home_controller.dart';
import 'package:app_rx_dart/di/app_dependency_injection.dart';
import 'package:app_rx_dart/presenter/views/home_view.dart';
import 'package:flutter/material.dart';

void main() {
  AppDependencyInjection.instance.init();
  log('run app');
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: HomeView(homeController: AppDependencyInjection.instance.getItInstance.get<HomeController>()),
    );
  }
}

