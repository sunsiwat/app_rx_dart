import 'dart:developer';

import 'package:app_rx_dart/core/disposer.dart';
import 'package:rxdart/rxdart.dart';

class HomeController extends Disposer {
  final BehaviorSubject<int> _dataStrObs = BehaviorSubject<int>();
  BehaviorSubject<int> get dataStrObs => _dataStrObs;

  void setInitialData() {
    Future.delayed(const Duration(seconds: 2), () => dataStrObs.sink.add(1));
  }

  @override
  void dispose() {
    _dataStrObs.close();
  }

  void addValue() {
    final currentValue = _dataStrObs.value;
    _dataStrObs.sink.add(currentValue + 1);
  }
}
