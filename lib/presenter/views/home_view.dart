import 'dart:developer';

import 'package:app_rx_dart/controller/home_controller.dart';
import 'package:flutter/material.dart';

class HomeView extends StatefulWidget {
  final HomeController homeController;
  const HomeView({super.key, required this.homeController});

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  late final HomeController controller;
  int value = 0;

  @override
  void initState() {
    controller = widget.homeController;
    controller.setInitialData();
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('RX Dart & Get it'),
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
      ),
      body: Center(
        child: StreamBuilder<int>(
          stream: controller.dataStrObs,
          // initialData: '',
          builder: (context, snapshot) {
            /// snapshot.data == null
            if (snapshot.hasData) {
              return Text(snapshot.data.toString());
            } else {
              return const CircularProgressIndicator();
            }
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          controller.addValue();
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
